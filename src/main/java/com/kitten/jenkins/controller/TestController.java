package com.kitten.jenkins.controller;

import org.springframework.web.bind.annotation.*;

@RestController
public class TestController {

    @GetMapping("/test-jenkins")
    public String testJenkins() {

        System.out.println("Hello Jenkins");

        return "Hello Jenkins";
    }
}
